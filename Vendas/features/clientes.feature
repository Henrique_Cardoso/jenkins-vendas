Feature: Gerencias clientes
  Como usuario, quero inserir e remover clientes

Scenario Outline: Incluir Cliente
    Given Estou na tela inicial
    Then Vou para tela de Clientes
    Then Clico botao adicionar
	  Then Digito novo cliente "<nome>","<telefone>","<endereco>","<observacoes>"
	  Then Gravo registro
Examples:
      |nome| telefone |endereco|  observacoes|
      |Zezinho| 21326545 |Rua do Ouvidor 312 Centro Macae | Crediario analisado, favor aumentar limite |

Scenario: Excluir Cliente
    Given Estou na tela inicial
    Then Vou para tela de Clientes
    Then Excluo primeiro registro
