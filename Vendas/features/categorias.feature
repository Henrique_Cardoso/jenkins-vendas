Feature: Gerencias categorias
  Como usuario, quero inserir e remover categoria

  Scenario Outline: Incluir Categoria
    Given Estou na tela inicial
    Then Vou para tela de Categorias
    Then Clico botao adicionar
	  Then Digito nova categoria <descricao>
	  Then Gravo registro
  Examples:
      | descricao    |
      | Zezinho      |


  Scenario: Excluir Categoria
    Given Estou na tela inicial
    Then Vou para tela de Categorias
    Then Excluo primeiro registro
