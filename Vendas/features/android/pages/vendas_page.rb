require 'calabash-android/abase'

class VendasPage < Calabash::ABase
	def trait 
		"textView marked:'Vendas'" 
	end # REAL TRAIT FOR CATEGORIAS PAGE


  def vai_para_pagina(pagina)
	rsi_embed_screenshot({:name => "estou_na_tela.png"})
    touch("ImageButton marked:'Open drawer navigation'")
	sleep(3)
	rsi_embed_screenshot({:name => "abriu_menu_drawer.png" })
    touch("AppCompatTextView marked:'#{pagina}'")
	sleep(3)
	rsi_embed_screenshot({:name => "fui_para_pagina_#{pagina}.png"})
  end


end
