require 'calabash-android/abase'

class CadastroPage < Calabash::ABase
  
  def click_excluir_primeiro_registro_cadastro()
	    sleep(3)
	    elementoAux = query "* id:'#{id_chave_da_lista}' index:0"
	    if elementoAux.size != 0
			touch("* id:'#{id_chave_da_lista}' index:0")
			sleep(3)
			rsi_embed_screenshot({:name => "#{qual_cadastro}_clicou_registro.png"})
		    touch("AppCompatButton marked:'Excluir'")
			sleep(3)
			rsi_embed_screenshot({:name => "#{qual_cadastro}_clicou_excluir.png"})
		    touch("AppCompatButton marked:'Sim'")
			sleep(3)
			retval = element_exists(cadastro_excluido_ok_query)  # Wishful thinkig? Expecting that, after 3 seconds, the result tiny textview will exist?
			rsi_embed_screenshot({:name => "#{qual_cadastro}_confirmou_excluir.png"})
		else
			fail("Não foi possivel encontrar o elemento [ #{cadastro_excluido_ok_query} ] na tela.")
		end
  end
  
  def click_botao_adicionar()
    sleep(3)
	touch("ActionMenuItemView id:'menu_inserir'")
	sleep(3)
	retval = element_exists("TextView marked:'#{marca_textview_novo_registro}'") #  # Wishful thinkig? Expecting that, after 3 seconds, the result tiny textview will exist?
	rsi_embed_screenshot({:name => "#{qual_cadastro}_clicou_adicionar.png"})
	if not retval
		fail("Error element [ #{marca_textview_novo_registro} ] not found")
	end
  end

  # FALTA A DIGITACAO DOS DADOS!

  def click_botao_gravar()
	 puts "comecando gravar"
     sleep(3)
	 touch("* id:'menu_gravar'")
	 sleep(2)
	 retval = element_exists(cadastro_incluido_ok_query)
	  # Wishful thinkig? Expecting that, after 3 seconds, the result tiny textview will exist?
	 puts "element_exists de texto #{cadastro_incluido_ok_query} retornou #{retval}"
	 rsi_embed_screenshot({:name => "#{qual_cadastro}_clicou_gravar.png"}) # Pode screenshot demorar e fazer desaparecer OK antes de wait?
	if not retval
		fail("Error element [ #{cadastro_incluido_ok_query} ] not found")
	end
  end
 
 end
  