require 'calabash-android/abase'
require "time"



class CategoriaPage < CadastroPage
	def trait 
		"textView marked:'Categorias'" 
	end # REAL TRAIT FOR CATEGORIAS PAGE

    def cadastro_excluido_ok_query
		"* marked:'Categoria excluída com sucesso!!!'"  ## CAUTION! Encoding, "í" !
	end

    def cadastro_incluido_ok_query
		"* marked:'Categoria cadastrada com sucesso!!!'"  ## CAUTION! Encoding, "í" !
	end

	
	def id_chave_da_lista
		"descricao"
	end
	
	def qual_cadastro
		"categorias"
	end
	
	def marca_textview_novo_registro
		"Nova Categoria"
	end

	def digitar_nova_categoria(descricao)
	  time = Time.now
	  enter_text("AppCompatEditText id:'descricao'",descricao+time.to_s)
	  hide_soft_keyboard
	  sleep(3)
	  rsi_embed_screenshot(:name => "#{qual_cadastro}_digitou_descricao.png")
	end
	
	
end