require 'calabash-android/abase'
require "time"

class ClientePage < CadastroPage
	def trait 
		"textView marked:'Clientes'" 
	end # REAL TRAIT FOR CATEGORIAS PAGE

    def cadastro_excluido_ok_query
		"* marked:'Cliente excluído com sucesso!!!'"  ## CAUTION! Encoding, "í" !
	end

    def cadastro_incluido_ok_query
		"* marked:'Cliente cadastrado com sucesso!!!'"   ## CAUTION! Encoding, "í" !
	end

	def id_chave_da_lista
		"nome"
	end
	
	def qual_cadastro
		"clientes"
	end

	def marca_textview_novo_registro
		"Novo Cliente"
	end
	
	def digitar_novo_cliente(nometxt, telefonetxt, enderecotxt, observacoestxt)
	  date=Time.new
	  touch "AppCompatEditText id:'nome'"
	  keyboard_enter_text(nometxt+date.to_s)
	  hide_soft_keyboard
	  sleep(3)
	  rsi_embed_screenshot({:name => "#{qual_cadastro}_digitou_nome.png"})
	  touch "AppCompatEditText id:'telefone'"
	  keyboard_enter_text(telefonetxt)
	  hide_soft_keyboard
	  sleep(3)
	  rsi_embed_screenshot({:name => "#{qual_cadastro}_digitou_telefone.png"})
	  touch "AppCompatEditText id:'endereco'"
	  keyboard_enter_text(enderecotxt)
	  hide_soft_keyboard
	  sleep(3)
	  rsi_embed_screenshot({:name => "#{qual_cadastro}_digitou_endereco.png"})
	  touch "AppCompatEditText id:'observacao'"
	  keyboard_enter_text(observacoestxt)
	  hide_soft_keyboard
	  sleep(3)
	  rsi_embed_screenshot({:name => "#{qual_cadastro}_digitou_observacoes.png"})
	end

	
end
