require 'calabash-android/operations'



#INSTALLATION_STATE = {
#    :installed => false
#}

Before do |scenario|
#by rbattaglia ini
  $nome_cenario_corrente = scenario.name
  $nome_cenario_corrente.sub! ', Examples (#1)' , ''
#by rbattaglia end

  #scenario_tags = scenario.source_tag_names
  #if !INSTALLATION_STATE[:installed]
  #    uninstall_apps #rbattaglia 2016-12-21 - DO NOT uninstall, HMM, HOW BAD IS THIS?
  #    install_app(ENV['TEST_APP_PATH'])
  #    install_app(ENV['APP_PATH'])
  #    INSTALLATION_STATE[:installed] = true
  #end

  #if scenario_tags.include?('@reinstall')
  #  clear_app_data
  #end
  #reinstall_apps
  start_test_server_in_background
end

After do |scenario|
  if scenario.failed?
	rsi_embed_screenshot({:name => "failed.png"})
	#screenshot -- ?? Not very hopeful an attempt, maybe not taking screenshot avoids abbend? Or is abbend cause of "cannot take screenshot"?
  end
  shutdown_test_server
end
