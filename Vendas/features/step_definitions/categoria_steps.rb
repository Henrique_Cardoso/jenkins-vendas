Then(/^Vou para tela de Categorias$/) do
  vendas_page = page(VendasPage).await(timeout: 30)
  vendas_page.vai_para_pagina('Categorias')
  @current_page = page(CategoriaPage).await(timeout: 30)
  # USING @current_page: some manuals say it is bad, but standar XAMARIN docs and x-platform use that technique
end

Then(/^Digito nova categoria ([^<]*)$/) do  | descricao | 
   @current_page.digitar_nova_categoria(descricao)
end
