Given(/^Estou na tela inicial$/) do
  sleep(10) # SLEEP importante para nao falhar imediatamente, para 1o cenario execytado. Sleep aqui para todos.
  @current_page = page(VendasPage).await(timeout: 30)
end

Then (/^Excluo primeiro registro$/) do
    @current_page.click_excluir_primeiro_registro_cadastro()
end

Then (/^Clico botao adicionar$/) do
    @current_page.click_botao_adicionar()
end

Then (/^Gravo registro$/) do
    @current_page.click_botao_gravar()
end
