Then(/^Vou para tela de Clientes$/) do  # TODO - por favor, faca "Vou Para Tela" receber parametro
  vendas_page = page(VendasPage).await(timeout: 30)
  vendas_page.vai_para_pagina('Clientes')
  @current_page = page(ClientePage).await(timeout: 30)
  # USING @current_page: some manuals say it is bad, but standar XAMARIN docs and x-platform use that technique
end

Then(/^Digito novo cliente "([^"]*)","([^"]*)","([^"]*)","([^"]*)"$/) do |nome, telefone, endereco, observacoes|
  @current_page.digitar_novo_cliente(nome, telefone, endereco, observacoes)
end
